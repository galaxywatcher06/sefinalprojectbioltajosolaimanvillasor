-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jan 04, 2019 at 10:09 AM
-- Server version: 5.7.19
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sharp_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `sharp_emp`
--

DROP TABLE IF EXISTS `sharp_emp`;
CREATE TABLE IF NOT EXISTS `sharp_emp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` text,
  `first_name` text NOT NULL,
  `middle_name` text,
  `last_name` text NOT NULL,
  `phone` int(11) NOT NULL,
  `employee_image` text NOT NULL,
  `id_type` text NOT NULL,
  `id_number` text NOT NULL,
  `id_card_image` text NOT NULL,
  `residence_address` text NOT NULL,
  `residence_location` text NOT NULL,
  `residence_direction` text NOT NULL,
  `residence_gps` text NOT NULL,
  `next_of_kin` text NOT NULL,
  `relationship` text NOT NULL,
  `phone_of_kin` text NOT NULL,
  `kin_residence` text NOT NULL,
  `kin_residence_direction` text NOT NULL,
  `date_employed` date NOT NULL,
  `job_type` text NOT NULL,
  `status` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sharp_emp`
--

INSERT INTO `sharp_emp` (`id`, `employee_id`, `first_name`, `middle_name`, `last_name`, `phone`, `employee_image`, `id_type`, `id_number`, `id_card_image`, `residence_address`, `residence_location`, `residence_direction`, `residence_gps`, `next_of_kin`, `relationship`, `phone_of_kin`, `kin_residence`, `kin_residence_direction`, `date_employed`, `job_type`, `status`) VALUES
(1, '001', 'torres St.', 'Tanker 10', 'Tango 01', 1, 'kdeYJ3LZ97a20wN_anime5.png', 'NHIS', '01', 'UEt9y7gdDVYOk1o_1472084611177.jpg', 'Fairlines', 'Davao City', 'somewhere down the road', '', 'Marjie Villasor', 'Mother', '09105668862', 'Langit', 'there and there', '2019-01-04', 'Sierra 10', 'former'),
(2, '03', 'Impyerno', 'tanker 10', 'Alpha 2', 2, 'osCaRQz0M49xlBy_23467029_1699630000057808_3721171286637331342_o.jpg', 'NHIS', '03', 'olbswz8StXqa2h1_23415226_1699629953391146_8188331876334014137_o.jpg', 'Hell', 'Heller', 'Hellest', '', 'Marjie Villasor', 'Sister', '09773273806', 'Catalunan Grande', 'somewhere down the road', '2019-01-04', 'Alpha 1, Bravo 2', 'former'),
(3, '23', 'weffsdfdf', 'sfdgdgdd', 'dfgdfgdfgdfgd', 2, 'RAvhC2STsw45yIi_23415426_1512155272198996_6108204209275105975_o.jpg', 'Passport', '77', 'tvpaCX2qK7zZQ9U_23415507_1699631303391011_5208901710407765429_o.jpg', 'fdvfdgdfg', 'sifjsdofisd', 'foisdfdsjfsdkfjdso', '', 'sdsfsf', 'nhdydh', '09773273806', 'jdjujjduuj', 'jndjfbkjhkdjffhkdj', '2019-01-04', 'vbtvv  rkfkg', 'former');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `firstname` text NOT NULL,
  `lastname` text NOT NULL,
  `username` text NOT NULL,
  `password` text NOT NULL,
  `accounttype` text NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `firstname`, `lastname`, `username`, `password`, `accounttype`) VALUES
(1, 'Maxwell', 'Morrison', 'xxx2xy', '10a55271c201e41913764ff95b33248b', 'Admin'),
(3, 'Maxwell', 'Morrison', 'admins', '4a7f064e93e8f12f3d364413af1d3b8c', 'Admin'),
(4, 'Mariane', 'Biol', 'biolbiol', '60d00097fd61224e4f0ac37b99bcc568', 'Employee');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
