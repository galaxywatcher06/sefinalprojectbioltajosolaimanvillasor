<?php
	include("../inc/header.php");
	
	include('../phpclasses/pagination.php');

    if($usertype != "Admin"){
        header("Location: ../dashboard");
    }
?>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Fire Brigade Record System</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="../css/jquery-ui.css" />
        <link rel="stylesheet" type="text/css" href="../css/style.css" />
    	<link href="../css/font-awesome.min.css" type="text/css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet">
        <script type="text/javascript" src="../js/jquery.min.js"></script>
        <script type="text/javascript" src="../js/jquery-ui.min.js"></script>
        <script type="text/javascript" src="../js/jquery.mask.js"></script>
    </head>
<body>
<body>
    <section class="side-menu fixed left">
        <div class="top-sec">
            <div class="dash_logo">
            </div>
            <p> FIRE BRIGADE RECORDING SYSTEM </p>
        </div>
        <ul class="nav">
        <li class="nav-item"><a href="../dashboard"><span class="nav-icon"><i class="fa fa-users"></i></span>All Staffs</a></li>
		
			<li class="nav-item"><a href="../dashboard/incident_information.php"><span class="nav-icon"><i class="fa fa-incident-info"></i></span>Incident Information</a></li>
			<li class="nav-item"><a href="../dashboard/current_employees.php"><span class="nav-icon"><i class="fa fa-check"></i></span>Current Staffs</a></li>
		
			<?php if($usertype == "Admin"){ ?>
				<li class="nav-item current"><a href="../dashboard/add_employee.php"><span class="nav-icon"><i class="fa fa-user-plus"></i></span>Add Staff</a></li>
				<li class="nav-item"><a href="../dashboard/add_user.php"><span class="nav-icon"><i class="fa fa-user"></i></span>Add User</a></li>
				<li class="nav-item"><a href="../dashboard/incident.php"><span class="nav-icon"><i class="fa fa-incident-info"></i></span>Add Info</a></li>
			<?php		} ?>
			<li class="nav-item"><a href="../dashboard/settings.php"><span class="nav-icon"><i class="fa fa-cog"></i></span>Settings</a></li>
			<li class="nav-item"><a href="../dashboard/logout.php"><span class="nav-icon"><i class="fa fa-sign-out"></i></span>Sign out</a></li>
		</ul>
        </section>
	<section class="contentSection right clearfix">
		<div class="displaySuccess"></div>
		<div class="container">
			<div class="wrapper add_employee clearfix">
				<div class="section_title">Incident Information</div>
				<form id="incidentinformation" class="clearfix" method="" action="">
					<div class="input-box input-small left">
						<label for="incident_id">Incident ID</label><br>
						<input type="text" class="inputField inc_id" name="incident_id">
						<div class="error inciderror"></div>
					</div>
					<div class="input-box input-small right">
						<label for="address">Address</label><br>
						<input type="text" class="inputField addresss" name="Address">
						<div class="error addresserror"></div>
					</div>
					<div class="input-box input-small left">
						<label for="incident_time">Time</label><br>
						<input type="text" class="inputField time" pname="time">
						<div class="error timeerror"></div>
					</div>
					<div class="input-box input-small right">
						<label for="incident_date">Date</label><br>
						<input type="text" id="datepicker" class="inputField date" name="date">
						<div class="error dateerror"></div>
					</div>
					<div class="input-box input-small left">
						<label for="truckname">Truck Name</label><br>
						<input type="text" class="inputField truckname" name="truckname">
						<div class="error trucknameerror"></div>
					</div>
					<div class="input-box input-small right">
						<label for="truck_driver">Truck Driver</label><br>
						<input type="text" class="inputField truck_driver" placeholder= "Call Sign" name="truck_driver">
						<div class="error truck_drivererror"></div>
					</div>
					<div class="input-box input-small left">
						<label for="crew_onboard">Crew Onboard</label><br>
						<input type="text"  class="inputField crew_onboard" name="crew_onboard">
						<div class="error crew_onboarderror"></div>
					</div>
                    <div class="input-box input-small right">
						<label for="crew_name">Crew Name(s)</label><br>
						<input type="text"  class="inputField crew_name" placeholder="Call Sign" name="crew_name">
						<div class="error crew_nameerror"></div>
					</div>
				
					<div class="input-box">
						<button type="submit" class="submitField">Add record</button>
					</div>
				</form>
			</div>
		</div>
	</section>
<script type="text/javascript" src="../js/global.js"></script>
</body>
</html>
    
    

