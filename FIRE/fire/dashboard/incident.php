<?php
	include("../inc/header.php");

	include('../phpclasses/pagination.php');


    $limit = 10;
	    
	//get number of rows
	$queryNum = $db_connect->query("SELECT COUNT(*) as postNum FROM incident_information ");
	$resultNum = $queryNum->fetch_assoc();
	$rowCount = $resultNum['postNum'];
										    
	//initialize pagination class
	$pagConfig = array(
		'totalRows' => $rowCount,
		'perPage' => $limit,
		'link_func' => 'incsearchFilter'
	);
	$pagination =  new Pagination($pagConfig);
										    
	//get rows

?>
	<section class="side-menu fixed left">
		<div class="top-sec">
			<div class="dash_logo">
			</div>			
			<p>  FIRE BRIGADE RECORDING SYSTEM</p>
			</div>
		<ul class="nav">
			<li class="nav-item"><a href="../dashboard"><span class="nav-icon"><i class="fa fa-users"></i></span>All Staffs</a></li>
			<li class="nav-item"><a href="../dashboard/incident_information.php"><span class="nav-icon"><i class="fa fa-incident-info"></i></span>Incident Information</a></li>
			<li class="nav-item current"><a href="../dashboard/current_employees.php"><span class="nav-icon"><i class="fa fa-check"></i></span>Current Staffs</a></li>
			

		
			<?php if($usertype == "Admin"){ ?>
				
				<li class="nav-item"><a href="../dashboard/add_employee.php"><span class="nav-icon"><i class="fa fa-user-plus"></i></span>Add Staff</a></li>
				<li class="nav-item"><a href="../dashboard/add_user.php"><span class="nav-icon"><i class="fa fa-user"></i></span>Add User</a></li>
				<li class="nav-item"><a href="../dashboard/incident.php"><span class="nav-icon"><i class="fa fa-incident-info"></i></span>Add Info</a></li>
			<?php		} ?>
			<li class="nav-item"><a href="../dashboard/settings.php"><span class="nav-icon"><i class="fa fa-cog"></i></span>Settings</a></li>
			<li class="nav-item"><a href="../dashboard/logout.php"><span class="nav-icon"><i class="fa fa-sign-out"></i></span>Sign out</a></li>
		</ul>
	</section>
	<section class="contentSection right clearfix">
		<div class="container">
			<div class="wrapper employee_list clearfix">
				<div class="section_title">Incident Information</div>
				<div class="top-bar">
					<div class="top-item">
						<form id="empFilter" method="post" action="">
							<input class="filterField filterVal" type="text" placeholder="Search Name" onkeyup="incsearchFilter()">
						</form>
					</div>
					<div class="top-item">
						<form id="empFilter" method="post" action="">
							<select class="sortField sortVal" onchange="currsearchFilter()">
								<option value="ASC">Newest</option>
								<option value="DESC">Oldest</option>
							</select>
						</form>
					</div>
				</div>
				<?php
					$getemp = mysqli_query($db_connect, "SELECT * FROM incident_information ");
					$getempcount = mysqli_num_rows($getemp);
					
				?>
				<ul class="emp_list">
					<li class="emp_list_head">
						<div class="emp_item_head incident_id">Incident ID</div>
						<div class="emp_item_head address">Address</div>
						<div class="emp_item_head">Incident Time</div>
						<div class="emp_item_head">Incident Date</div>
						<div class="emp_item_head emp_status">Truck Name</div>
						<div class="emp_item_head">Driver Incharge</div>
                        <div class="emp_item_head">Crew Onboard</div>
                        <div class="emp_item_head">Crew Name(s)</div>
					</li>
					<div id="displaincidentinformationList">
						<?php
							if($getempcount >= 8 ){
								while($fetch = mysqli_real_escape_string($getemp)){
									$incident_id = $fetch['incident_id'];
									$incident_time = $fetch['incident_time'];
									$incident_date = $fetch['incident_date'];
									$truckname = $fetch['truckname'];
									$truck_driver = $fetch['truck_driver'];
									$crew_onboard = $fetch['crew_onboard'];
									$crew_name = $fetch['crew_name'];
									

									$incident_date = date("jS F Y", strtotime($incident_date));

									if($incident_id == ""){
										if($usertype == "Admin"){
											echo '										
												<li class="emp_item">
													<div class="emp_column emp_id">'.$incident_id.'</div>
													<div class="emp_column ">'.$address.'</div>
													<div class="emp_column ">'.$incident_time.'</div>
													<div class="emp_column">'.$incident_date.'</div>
													<div class="emp_column">'.$truckname.'</div>
                                                    <div class="emp_column">'.$truck_driver.'</div>
                                                    <div class="emp_column">'.$crew_onboard.'</div>
                                                    <ddiv class="emp_column">'.$crew_name.'</div>
													<div class="emp_column">
														<ul class="action_list">
															<li class="action_item action_view" data-id="'.$id.'" title="View"><i class="fa fa-eye"></i></li>
															<li class="action_item action_edit" data-id="'.$id.'" title="Edit"><i class="fa fa-pencil-square-o"></i></li>
															<li class="action_item action_delete" data-id="'.$id.'" title="Delete"><i class="fa fa-trash-o"></i></li>
														</ul>
													</div>
												</li>
											';
										} else {
											echo '										
                                            <li class="emp_item">
                                            <div class="emp_column emp_id">'.$incident_id.'</div>
											<div class="emp_column ">'.$address.'</div>
											<div class="emp_column ">'.$incident_time.'</div>
                                            <div class="emp_column">'.$incident_date.'</div>
                                            <div class="emp_column">'.$truckname.'</div>
                                            <div class="emp_column">'.$truck_driver.'</div>
                                            <div class="emp_column">'.$crew_onboard.'</div>
                                            <ddiv class="emp_column">'.$crew_name.'</div>
                                            <div class="emp_column">
														<ul class="action_list">
															<li class="action_item action_view" data-id="'.$id.'" title="View"><i class="fa fa-eye"></i></li>
														</ul>
													</div>
												</li>
											';											
										}
									} else {
										if($usertype == "Admin"){
											echo '										
                                            <li class="emp_item">
                                            <div class="emp_column emp_id">'.$incident_id.'</div>
											<div class="emp_column ">'.$address.'</div>
											<div class="emp_column ">'.$incident_time.'</div>
                                            <div class="emp_column">'.$incident_date.'</div>
                                            <div class="emp_column">'.$truckname.'</div>
                                            <div class="emp_column">'.$truck_driver.'</div>
                                            <div class="emp_column">'.$crew_onboard.'</div>
                                            <ddiv class="emp_column">'.$crew_name.'</div>
                                            <div class="emp_column">
														<ul class="action_list">
															<li class="action_item action_view" data-id="'.$id.'" title="View"><i class="fa fa-eye"></i></li>
															<li class="action_item action_edit" data-id="'.$id.'" title="Edit"><i class="fa fa-pencil-square-o"></i></li>
															<li class="action_item action_delete" data-id="'.$id.'" title="Delete"><i class="fa fa-trash-o"></i></li>
														</ul>
													</div>
												</li>
											';
										} else {

											echo '										
                                            <li class="emp_item">
                                            <div class="emp_column emp_id">'.$incident_id.'</div>
											<div class="emp_column ">'.$address.'</div>
											<div class="emp_column ">'.$incident_time.'</div>
                                            <div class="emp_column">'.$incident_date.'</div>
                                            <div class="emp_column">'.$truckname.'</div>
                                            <div class="emp_column">'.$truck_driver.'</div>
                                            <div class="emp_column">'.$crew_onboard.'</div>
                                            <ddiv class="emp_column">'.$crew_name.'</div>
                                            <div class="emp_column">
														<ul class="action_list">
															<li class="action_item action_view" data-id="'.$id.'" title="View"><i class="fa fa-eye"></i></li>
														</ul>
													</div>
												</li>
											';
										}
									}
								}
								echo $pagination->createLinks();
							} else {
								echo '<li class="emp_item"> No record found</li>';
							}
						?>
					</div>
				</ul>
			</div>
		</div>
		
		<div class="del_modal">
			<div class"inner_section">
				<div class="delcontainer">
					<div class="del_title">Delete Record</div>
					<div class="del_warning"></div>
					<div class="btnwrapper">
						<span class="delbtn yesbtn" data-id="">Yes</span>
						<span class="delbtn nobtn">No</span>
					</div>
				</div>
			</div>
		</div>
	</section>
<script type="text/javascript" src="../js/global.js"></script>
</body>
</html>